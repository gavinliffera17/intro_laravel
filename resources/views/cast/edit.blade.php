@extends('layout.master')
@section('judul')
Halaman Edit Cast {{$cast->nama}}
@endsection


@section('content')


<div>
        <h2>Edit Cast{{$cast->id}}</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Cast</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}"  placeholder="Masukkan Nama Genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur Cast</label>
                <input type="number" class="form-control" name="umur" value="{{$cast->umur}}"  placeholder="Masukkan Umur Cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio Cast</label>
                <textarea  class="form-control" name="bio" value="{{$cast->bio}}"  cols="10" rows="10" placeholder="Masukkan Bio Cast"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection