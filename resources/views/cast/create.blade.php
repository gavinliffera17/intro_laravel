@extends('layout.master')
@section('judul')
Membuat Cast
@endsection


@section('content')
<form action="/cast" method="POST">
    @csrf
  <div class="form-group">
    <label>Nama Cast</label>
    <input type="text" name="nama" class="form-control">
    {{-- <small id="genreHelp" class="form-text text-muted">Masukkan Jenis Genre Film</small> --}}
  </div>
    @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
  <div class="form-group">
    <label>Umur Cast</label>
    <input type="number" name="umur" class="form-control">
    {{-- <small id="umurHelp" class="form-text text-muted">Masukkan Jenis Genre Film</small> --}}
  </div>
    @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
  <div class="form-group">
    <label>Bio Cast</label>
    <textarea name="bio" class="form-control" cols="10" rows="10"></textarea>
    {{-- <small id="genreHelp" class="form-text text-muted">Masukkan Jenis Genre Film</small> --}}
  </div>
    @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
  {{-- <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> --}}
  <button type="submit" class="btn btn-primary">Simpan</button>
</form>

@endsection


