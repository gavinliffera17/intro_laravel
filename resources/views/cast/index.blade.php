@extends('layout.master')
@section('judul')
Tabel Cast
@endsection


@section('content')
<a href="/cast/create" class="btn btn-success my-1">Tambah Data</a>
 
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  @forelse ($cast as $key => $item)
     <tr>
        <td>{{$key + 1}}</th>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>{{$item->bio}}</td>
       <td>
       <a href="/cast/{{$item->id}}" class="btn btn-info">Detail</a>
       <a href="/cast/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
        <form action="/cast/{{$item->id}}" method="POST">
            @csrf
            @method('DELETE')
          <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
        </td>
     </tr>
  @empty
      <tr>
          <td>
        Data Masih Kososng      
          </td>
      </tr>
  @endforelse
  </tbody>
</table>

@endsection