<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>

<body>
    <h1>Buat Account Baru !!!</h1>
    <h2><b>Sign Up Form</b></h2>
    <form method="post" action="/welcome">
        @csrf
        <fieldset>
            <legend> <b>Wajib Diisi !!</b></legend>
            <label for="namadepan">First Name :</label> <br>
            <input type="text" placeholder="First Name" name="namadepan"><br>
            <label for="namablkg">Last Name :</label> <br>
            <input type="text" placeholder="Last Name" name="namablkg"> <br>

            <label for="gender">Gender</label>
            <input type="radio" name="gender" value="male"> Male
            <input type="radio" name="gender" value="female"> Female
            <input type="radio" name="gender" value="other"> Other <br>

            <label for="nationality">Nationality</label>
            <select name="nationality" id="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="American">American</option>
                <option value="British">British</option>
            </select>
            <br>
            <label for="Bio">Bio : </label> <br>
            <textarea name="Bio" id="Bio" cols="30" rows="10" placeholder="Bio"></textarea>
            <br>
        </fieldset>
        <br>
        <input type="submit" name="sign" value="SIGN UP"> <br>
    </form>

</body>


</html>