@extends('layout.master')
@section('judul')
Halaman Home    
@endsection


@section('content')
    
<div>
    
        <h1>Media Online</h1>

        <h3>Social Media Developer</h3>
        
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

        <p><b>Benefit Join di Media Online</b></p>

        <ul>
            <li>Mendapatkan Motivasi dari sesama para Developer</li>
            <li>Sharing Knowledge</li>
            <li>Dibuat oleh Calon Web Developer Terbaik</li>
        </ul>
        
        <p><b>Cara Bergabung ke Media Online</b></p>
        <ol>
        <li>Mengunjungi Website ini </li>
        <li>Mendaftarkan di <a href="/register" > Form Sign Up</a></li>
        <li>Selesai</li>
        </ol>
    </div>

    @endsection
