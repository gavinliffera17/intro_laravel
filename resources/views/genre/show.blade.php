@extends('layout.master')
@section('judul')
Halaman Detail Genre {{$genre->nama}}
@endsection


@section('content')

<h2>Menampilkan Genre {{$genre->id}}</h2>
<h4>{{$genre->nama}}</h4>
<a href="/genre" class="btn btn-warning"> Kembali</a>


@endsection