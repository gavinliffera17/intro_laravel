@extends('layout.master')
@section('judul')
Halaman Edit Genre {{$genre->nama}}
@endsection


@section('content')


<div>
        <h2>Edit Genre {{$genre->id}}</h2>
        <form action="/genre/{{$genre->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Genre</label>
                <input type="text" class="form-control" name="nama" value="{{$genre->nama}}"  placeholder="Masukkan Nama Genre">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection