@extends('layout.master')
@section('judul')
Membuat Genre
@endsection


@section('content')
<form action="/genre" method="POST">
    @csrf
  <div class="form-group">
    <label>Nama Genre</label>
    <input type="text" name="nama" class="form-control">
    <small id="genreHelp" class="form-text text-muted">Masukkan Jenis Genre Film</small>
  </div>
    @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
  {{-- <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> --}}
  <button type="submit" class="btn btn-primary">Simpan</button>
</form>

@endsection


